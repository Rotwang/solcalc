#![allow(non_snake_case)]
#![allow(clippy::collapsible_if)]
mod azel;
pub use azel::AzimuthElevation;

mod sunriseset;
pub use sunriseset::SunriseSet;

mod solcalcdate;
mod utils;

pub fn calc_time_julian_cent(jd: f64) -> f64 {
    (jd - 2451545.0) / 36525.0
}

pub fn get_jd(mut year: f64, mut month: f64, day: f64) -> f64 {
    if month <= 2.0 {
        year -= 1.0;
        month += 12.0;
    }

    let a = (year / 100.0).floor();
    let b = 2.0 - a + (a / 4.0).floor();
    (365.25 * (year + 4716.0)).floor() + (30.6001 * (month + 1.0)).floor() + day + b - 1524.5
}

pub fn calc_equation_of_time(t: f64) -> f64 {
    let epsilon = utils::calc_obliquity_correction(t);
    let l0 = utils::calc_geom_mean_long_sun(t);
    let e = utils::calc_eccentricity_earth_orbit(t);
    let m = utils::calc_geom_mean_anomaly_sun(t);

    let mut y = (epsilon.to_radians() / 2.0).tan();
    y *= y;

    let sin2l0 = (2.0 * l0.to_radians()).sin();
    let sinm = m.to_radians().sin();
    let cos2l0 = (2.0 * l0.to_radians()).cos();
    let sin4l0 = (4.0 * l0.to_radians()).sin();
    let sin2m = (2.0 * m.to_radians()).sin();

    let Etime = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0
        - 0.5 * y * y * sin4l0
        - 1.25 * e * e * sin2m;
    Etime.to_degrees() * 4.0 // in minutes of time
}

pub fn calc_sun_declination(t: f64) -> f64 {
    let e = utils::calc_obliquity_correction(t);
    let lambda = utils::calc_sun_apparent_long(t);
    let sint = e.to_radians().sin() * lambda.to_radians().sin();
    sint.asin().to_degrees() // in degrees
}

pub fn calc_sol_noon(jd: f64, longitude: f64, timezone: f64) -> f64 {
    let tnoon = calc_time_julian_cent(jd - longitude / 360.0);
    let mut eq_time = calc_equation_of_time(tnoon);
    let sol_noon_offset = 720.0 - (longitude * 4.0) - eq_time; // in minutes
    let newt = calc_time_julian_cent(jd + sol_noon_offset / 1440.0);
    eq_time = calc_equation_of_time(newt);
    let mut sol_noon_local = 720.0 - (longitude * 4.0) - eq_time + (timezone * 60.0); // in minutes
    while sol_noon_local < 0.0 {
        sol_noon_local += 1440.0;
    }
    while sol_noon_local >= 1440.0 {
        sol_noon_local -= 1440.0;
    }

    sol_noon_local
}
