use crate::{
    calc_time_julian_cent,
    utils::{calc_JD_of_next_prev_rise_set, calc_doy_from_JD, calc_sunrise_set_UTC},
    AzimuthElevation,
};

#[derive(Debug)]
pub struct SunriseSet {
    pub jday: f64,
    pub timelocal: f64,
    pub azimuth: f64,
}

impl SunriseSet {
    // rise = true for sunrise, false for sunset
    pub fn new(rise: bool, JD: f64, latitude: f64, longitude: f64, timezone: f64) -> Self {
        let time_UTC = calc_sunrise_set_UTC(rise, JD, latitude, longitude);
        let new_time_UTC = calc_sunrise_set_UTC(rise, JD + time_UTC / 1440.0, latitude, longitude);
        if new_time_UTC.is_finite() {
            let mut timelocal = new_time_UTC + (timezone * 60.0);
            let rise_T = calc_time_julian_cent(JD + new_time_UTC / 1440.0);
            let rise_AzEl = AzimuthElevation::new(rise_T, timelocal, latitude, longitude, timezone);
            let azimuth = rise_AzEl.azimuth;
            let mut jday = JD;
            if (timelocal < 0.0) || (timelocal >= 1440.0) {
                let increment = if timelocal < 0.0 { 1.0 } else { -1.0 };
                while (timelocal < 0.0) || (timelocal >= 1440.0) {
                    timelocal += increment * 1440.0;
                    jday -= increment;
                }
            }
            SunriseSet {
                jday,
                timelocal,
                azimuth,
            }
        } else {
            // no sunrise/set found
            let azimuth = -1.0;
            let timelocal = 0.0;
            let doy = calc_doy_from_JD(JD);
            let jday = if ((latitude > 66.4) && (doy > 79.0) && (doy < 267.0))
                || ((latitude < -66.4) && ((doy < 83.0) || (doy > 263.0)))
            {
                //previous sunrise/next sunset
                calc_JD_of_next_prev_rise_set(!rise, rise, JD, latitude, longitude, timezone)
            } else {
                //previous sunset/next sunrise
                calc_JD_of_next_prev_rise_set(rise, rise, JD, latitude, longitude, timezone)
            };
            SunriseSet {
                jday,
                timelocal,
                azimuth,
            }
        }
    }
}
