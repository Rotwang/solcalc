use crate::{
    calc_equation_of_time, calc_sun_declination, calc_time_julian_cent, solcalcdate::SolCalcDate,
};

pub(crate) fn calc_geom_mean_long_sun(t: f64) -> f64 {
    let mut L0 = 280.46646 + t * (36000.76983 + t * (0.0003032));
    while L0 > 360.0 {
        L0 -= 360.0;
    }
    while L0 < 0.0 {
        L0 += 360.0;
    }
    L0 // in degrees
}

pub(crate) fn calc_obliquity_correction(t: f64) -> f64 {
    let e0 = calc_mean_obliquity_of_ecliptic(t);
    let omega = 125.04 - 1934.136 * t;
    e0 + 0.00256 * omega.to_radians().cos() // in degrees
}

pub(crate) fn calc_sun_rad_vector(t: f64) -> f64 {
    let v = calc_sun_true_anomaly(t);
    let e = calc_eccentricity_earth_orbit(t);
    (1.000001018 * (1.0 - e * e)) / (1.0 + e * v.to_radians().cos()) // in AUs
}

pub(crate) fn calc_refraction(elev: f64) -> f64 {
    if elev > 85.0 {
        0.0
    } else {
        let te = elev.to_radians().tan();
        let correction = if elev > 5.0 {
            58.1 / te - 0.07 / (te * te * te) + 0.000086 / (te * te * te * te * te)
        } else if elev > -0.575 {
            1735.0 + elev * (-518.2 + elev * (103.4 + elev * (-12.79 + elev * 0.711)))
        } else {
            -20.774 / te
        };
        correction / 3600.0
    }
}

pub(crate) fn calc_sunrise_set_UTC(rise: bool, JD: f64, latitude: f64, longitude: f64) -> f64 {
    let t = calc_time_julian_cent(JD);
    let eq_time = calc_equation_of_time(t);
    let solar_dec = calc_sun_declination(t);
    let mut hour_angle = calc_hour_angle_sunrise(latitude, solar_dec);
    if !rise {
        hour_angle = -hour_angle;
    }
    let delta = longitude + hour_angle.to_degrees();
    720.0 - (4.0 * delta) - eq_time // in minutes
}

pub(crate) fn calc_doy_from_JD(jd: f64) -> f64 {
    let date = SolCalcDate::new(jd);

    let k = if is_leap_year(date.year) { 1.0 } else { 2.0 };
    ((275.0 * date.month) / 9.0).floor() - k * ((date.month + 9.0) / 12.0).floor() + date.day - 30.0
}

pub(crate) fn calc_JD_of_next_prev_rise_set(
    next: bool,
    rise: bool,
    JD: f64,
    latitude: f64,
    longitude: f64,
    tz: f64,
) -> f64 {
    let mut julianday = JD;
    let increment = if next { 1.0 } else { -1.0 };
    let mut time = calc_sunrise_set_UTC(rise, julianday, latitude, longitude);

    while !time.is_finite() {
        julianday += increment;
        time = calc_sunrise_set_UTC(rise, julianday, latitude, longitude);
    }
    let mut time_local = time + tz * 60.0;
    while (time_local < 0.0) || (time_local >= 1440.0) {
        let incr = if time_local < 0.0 { 1.0 } else { -1.0 };
        time_local += incr * 1440.0;
        julianday -= incr;
    }

    julianday
}

pub(crate) fn calc_eccentricity_earth_orbit(t: f64) -> f64 {
    0.016708634 - t * (0.000042037 + 0.0000001267 * t) // unitless
}

pub(crate) fn calc_geom_mean_anomaly_sun(t: f64) -> f64 {
    357.52911 + t * (35999.05029 - 0.0001537 * t) // in degrees
}

pub(crate) fn calc_sun_apparent_long(t: f64) -> f64 {
    let o = calc_sun_true_long(t);
    let omega = 125.04 - 1934.136 * t;
    o - 0.00569 - 0.00478 * omega.to_radians().sin() // in degrees
}

fn is_leap_year(yr: f64) -> bool {
    (yr % 4.0 == 0.0 && yr % 100.0 != 0.0) || yr % 400.0 == 0.0
}

fn calc_mean_obliquity_of_ecliptic(t: f64) -> f64 {
    let seconds = 21.448 - t * (46.8150 + t * (0.00059 - t * (0.001813)));
    23.0 + (26.0 + (seconds / 60.0)) / 60.0 // in degrees
}

fn calc_sun_true_long(t: f64) -> f64 {
    let l0 = calc_geom_mean_long_sun(t);
    let c = calc_sun_eq_of_center(t);
    l0 + c // in degrees
}

fn calc_sun_true_anomaly(t: f64) -> f64 {
    let m = calc_geom_mean_anomaly_sun(t);
    let c = calc_sun_eq_of_center(t);
    m + c // in degrees
}

fn calc_sun_eq_of_center(t: f64) -> f64 {
    let m = calc_geom_mean_anomaly_sun(t);
    let mrad = m.to_radians();
    let sinm = mrad.sin();
    let sin2m = (mrad + mrad).sin();
    let sin3m = (mrad + mrad + mrad).sin();
    sinm * (1.914602 - t * (0.004817 + 0.000014 * t))
        + sin2m * (0.019993 - 0.000101 * t)
        + sin3m * 0.000289 // in degrees
}

fn calc_hour_angle_sunrise(lat: f64, solar_dec: f64) -> f64 {
    let lat_rad = lat.to_radians();
    let sd_rad = solar_dec.to_radians();
    let HA_arg = 90.833f64.to_radians().cos() / (lat_rad.cos() * sd_rad.cos())
        - lat_rad.tan() * sd_rad.tan();
    HA_arg.acos() // in radians (for sunset, use -HA)
}
