use crate::{
    utils::{calc_refraction, calc_sun_rad_vector},
    {calc_equation_of_time, calc_sun_declination},
};

#[derive(Debug)]
pub struct AzimuthElevation {
    pub azimuth: f64,
    pub elevation: f64,
}

impl AzimuthElevation {
    pub fn new(T: f64, localtime: f64, latitude: f64, longitude: f64, zone: f64) -> Self {
        let eq_time = calc_equation_of_time(T);
        let theta = calc_sun_declination(T);

        let solar_time_fix = eq_time + 4.0 * longitude - 60.0 * zone;
        let _earth_rad_vec = calc_sun_rad_vector(T);
        let mut true_solar_time = localtime + solar_time_fix;
        while true_solar_time > 1440.0 {
            true_solar_time -= 1440.0;
        }
        let mut hour_angle = true_solar_time / 4.0 - 180.0;
        if hour_angle < -180.0 {
            hour_angle += 360.0;
        }
        let ha_rad = hour_angle.to_radians();
        let mut csz = latitude.to_radians().sin() * theta.to_radians().sin()
            + latitude.to_radians().cos() * theta.to_radians().cos() * ha_rad.cos();

        if csz > 1.0 {
            csz = 1.0;
        } else if csz < -1.0 {
            csz = -1.0;
        }
        let zenith = csz.acos().to_degrees();
        let az_denom = latitude.to_radians().cos() * zenith.to_radians().sin();
        let mut azimuth = if az_denom.abs() > 0.001 {
            let mut az_rad = ((latitude.to_radians().sin() * zenith.to_radians().cos())
                - theta.to_radians().sin())
                / az_denom;
            if az_rad.abs() > 1.0 {
                if az_rad < 0.0 {
                    az_rad = -1.0;
                } else {
                    az_rad = 1.0;
                }
            }
            let mut azimuth = 180.0 - az_rad.acos().to_degrees();
            if hour_angle > 0.0 {
                azimuth = -azimuth;
            }
            azimuth
        } else {
            if latitude > 0.0 {
                180.0
            } else {
                0.0
            }
        };
        if azimuth < 0.0 {
            azimuth += 360.0;
        }
        let exoatm_elevation = 90.0 - zenith;

        // Atmospheric Refraction correction
        let refraction_correction = calc_refraction(exoatm_elevation);

        let solar_zen = zenith - refraction_correction;
        let elevation = 90.0 - solar_zen;

        AzimuthElevation { azimuth, elevation }
    }
}
