#[derive(Debug)]
pub(crate) struct SolCalcDate {
    pub year: f64,
    pub month: f64,
    pub day: f64,
}

impl SolCalcDate {
    pub(crate) fn new(jd: f64) -> Self {
        let z = (jd + 0.5).floor();
        let f = (jd + 0.5) - z;
        let A = if z < 2299161.0 {
            z
        } else {
            let alpha = ((z - 1867216.25) / 36524.25).floor();
            z + 1.0 + alpha - (alpha / 4.0).floor()
        };
        let B = A + 1524.0;
        let C = ((B - 122.1) / 365.25).floor();
        let D = (365.25 * C).floor();
        let E = ((B - D) / 30.6001).floor();
        let day = B - D - (30.6001 * E).floor() + f;
        let month = if E < 14.0 { E - 1.0 } else { E - 13.0 };
        let year = if month > 2.0 { C - 4716.0 } else { C - 4715.0 };

        SolCalcDate { year, month, day }
    }
}
