use chrono::{Datelike, NaiveDateTime, Timelike};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

type Measurements = HashMap<NaiveDateTime, HashMap<String, ComparisonInfo>>;

macro_rules! r {
    ($f:expr) => {
        ($f * 100.0).round() as u64
    };
}

macro_rules! make_compare {
    ($place:expr, $when:expr) => {
        macro_rules! compare {
            ($reference:expr, $sut:expr) => {
                assert_eq!($reference, $sut, "{}: {:?} | {:?}", $place, $when, $sut)
            };
        }
    };
}

#[derive(Debug, Serialize, Deserialize)]
struct ComparisonData {
    measurements: Measurements,
    places: HashMap<String, ComparisonPlace>,
}

#[derive(Debug, Serialize, Deserialize)]
struct ComparisonInfo {
    azimuth: f64,
    elevation: f64,
    noon: f64,
    eqtime: f64,
    decl: f64,
    set: RiseSet,
    rise: RiseSet,
}

#[derive(Debug, Serialize, Deserialize)]
struct RiseSet {
    jday: f64,
    timelocal: f64,
    azimuth: f64,
}

#[derive(Debug, Serialize, Deserialize)]
struct ComparisonPlace {
    name: String,
    lat: f64,
    lng: f64,
    zone: f64,
}

impl std::cmp::PartialEq<solcalc::SunriseSet> for RiseSet {
    fn eq(&self, other: &solcalc::SunriseSet) -> bool {
        r!(self.jday) == r!(other.jday)
            && r!(self.timelocal) == r!(other.timelocal)
            && r!(self.azimuth) == r!(other.azimuth)
    }
}

#[test]
fn acceptance() {
    let comp_data_file = include_bytes!("upstream-comparison-data.json");
    let comp_data: ComparisonData = serde_json::from_slice(comp_data_file).unwrap();

    for (when, measurements) in comp_data.measurements {
        for (place, measurement) in measurements {
            let time_local =
                when.hour() as f64 * 60.0 + when.minute() as f64 + when.second() as f64 / 60.0;
            let loc = comp_data.places.get(&place).unwrap();
            let jday = solcalc::get_jd(when.year() as f64, when.month() as f64, when.day() as f64);
            let total = jday + time_local / 1440.0 - loc.zone / 24.0;
            #[allow(non_snake_case)]
            let T = solcalc::calc_time_julian_cent(total);

            make_compare!(place, when);

            compare!(r!(measurement.decl), r!(solcalc::calc_sun_declination(T)));

            compare!(
                r!(measurement.eqtime),
                r!(solcalc::calc_equation_of_time(T))
            );

            compare!(
                r!(measurement.noon),
                r!(solcalc::calc_sol_noon(jday, loc.lng, loc.zone))
            );

            compare!(
                measurement.rise,
                solcalc::SunriseSet::new(true, jday, loc.lat, loc.lng, loc.zone)
            );

            compare!(
                measurement.set,
                solcalc::SunriseSet::new(false, jday, loc.lat, loc.lng, loc.zone)
            );

            compare!(
                r!(measurement.azimuth),
                r!(
                    solcalc::AzimuthElevation::new(T, time_local, loc.lat, loc.lng, loc.zone)
                        .azimuth
                )
            );

            compare!(
                r!(measurement.elevation),
                r!(
                    solcalc::AzimuthElevation::new(T, time_local, loc.lat, loc.lng, loc.zone)
                        .elevation
                )
            );
        }
    }
}
