// meant to be run in the browser at
// https://www.esrl.noaa.gov/gmd/grad/solcalc/
//location.reload()

function arrToObj(array, key) {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item,
    };
  }, initialValue);
}

function get_msmnts(y, m, d, h, min, sec, z, lat, lng) {
  let time_local = h * 60 + min + sec / 60;
  let jday = getJD(y, m, d);
  let total = jday + time_local / 1440.0 - z / 24.0;
  let T = calcTimeJulianCent(total);
  let azel = calcAzEl(T, time_local, lat, lng, z);
  let stuff = {
    noon: calcSolNoon(jday, lng, z),
    rise: calcSunriseSet(1, jday, lat, lng, z),
    set: calcSunriseSet(0, jday, lat, lng, z),
    eqtime: calcEquationOfTime(T),
    decl: calcSunDeclination(T)
  };
  return { ...azel, ...stuff };
}

//moment.tz("2011-10-05T14:48:00.000Z", "UTC").format()
function generate() {
  
  let places_arr = USCities.concat(WorldCities).concat(obs).concat(surfrad).concat(sites);
  let places = arrToObj(places_arr, "name");
  let dta = {};
  let res = { measurements: dta, places: places };
  for (x = 0; x < 1000; x++) {
    let t = moment.unix(Math.floor(Math.random() * 63145439818 - 30610223819));
    let t_str = t.utc().format("YYYY-MM-DDTHH:mm:ss");
    dta[t_str] = {}
    for (l of places_arr) {
      let foo = { [l.name]: get_msmnts(t.year(), t.month() + 1, t.date(), t.hour(), t.minute(), t.second(), l.zone, l.lat, l.lng) };
      let bar = dta[t_str];
      dta[t_str] = { ...bar, ...foo };
    }
  }
  return res;
}

generate()